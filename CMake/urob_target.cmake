########################################################################################
#
# Copyright 2022 syntakt
# All rights reserved
#
########################################################################################

include(CMakePackageConfigHelpers)

function (urob_add_target target dir)
	cmake_parse_arguments(ARG "" "" "DEFS;DEPS" ${ARGN})

	file(GLOB_RECURSE sources "${dir}/src/*.cc")
	if (sources)
		target_sources(${target} PRIVATE ${sources})
	endif ()

	target_compile_definitions(${target} PUBLIC ${ARG_DEFS})
	target_include_directories(${target} PUBLIC $<BUILD_INTERFACE:${dir}/include> $<INSTALL_INTERFACE:include>)
	target_link_libraries(${target} INTERFACE ${target})

	foreach (dep ${ARG_DEPS})
		set(dep_pkg ${dep})
		string(FIND ${dep} "::" double_colon_pos)
		if (NOT ${double_colon_pos} EQUAL -1)
			string(SUBSTRING ${dep} 0 ${double_colon_pos} dep_pkg)
			math(EXPR dep_comp_pos "${double_colon_pos} + 2")
			string(SUBSTRING ${dep} ${dep_comp_pos} -1 dep_comp)
		endif ()
		list(APPEND dep_pkgs ${dep_pkg})
		list(APPEND ${dep_pkg}_comps ${dep_comp})
	endforeach ()
	list(REMOVE_DUPLICATES dep_pkgs)

	foreach (dep_pkg ${dep_pkgs})
		if (NOT ${dep_pkg} STREQUAL ${PROJECT_NAME})
			find_package(${dep_pkg} REQUIRED COMPONENTS ${${dep_pkg}_comps})
		endif ()
	endforeach ()
	target_link_libraries(${target} PUBLIC ${ARG_DEPS})
endfunction ()

function (urob_add_library name dir)
	set(target ${PROJECT_NAME}-${name})
	add_library(${target})
	add_library(${PROJECT_NAME}::${name} ALIAS ${target})
	urob_add_target(${target} "${dir}" "${ARGN}")

	get_property(target_list DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}" PROPERTY targets)
	list(APPEND target_list ${target})
	set_property(DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}" PROPERTY targets ${target_list})

	file(GLOB_RECURSE headers "${dir}/include/${name}/*.h" "${dir}/include/${name}/*.inl")
	target_sources(${target} PUBLIC FILE_SET HEADERS BASE_DIRS "${dir}/include" FILES "${headers}")
endfunction ()

function (urob_add_executable name dir)
	set(target ${PROJECT_NAME}-${name})
	add_executable(${target})
	urob_add_target(${target} "${dir}" "${ARGN}")
	cmake_path(RELATIVE_PATH dir BASE_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}" OUTPUT_VARIABLE output_dir)
	set_target_properties(${target} PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/${output_dir}")
endfunction ()

function (urob_add_test name dir)
	urob_add_executable(${name} "${dir}" ${ARGN})
	set(target ${PROJECT_NAME}-${name})
	add_test(NAME ${target} COMMAND $<TARGET_FILE:${target}>)
endfunction ()
