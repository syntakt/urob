urob
===
A collection of CMake helper utilities for project build solution generation.

---

Usage
---
Every urob-based C++ project requires that a CMake cache entry `UROB_CMAKE_DIR` is defined and points to a directory containing the required CMake modules ahead of configuring:
1. download the repository to an arbitrary destination, e.g. `/path/to/urob`,
2. run CMake
    * from the command line, add an argument, i.e. `cmake /path/to/project -DUROB_CMAKE_DIR=/path/to/urob/CMake`,
    * using GUI, set `UROB_CMAKE_DIR` as a `PATH` entry of value `/path/to/urob/CMake`.

If the project you are building depends on other urob-based libraries, CMake will use its `FetchContent` capabilities to retrieve them. The dependencies are then built as part of the main solution.

Development
---
A new project should adhere to the filesystem hierarchy expected by the provided set of modules. See the following example.
```text
<package_name>
├── [examples]
│   ├── <example_name>
│   │   └── src
│   │       ├── <example_source.cc>
│   │       └── ...
│   ├── ...
│   └── CMakeLists.txt
├── libs
│   ├── <library_name>
│   │   ├── include
│   │   │   └── <library_name>
│   │   │       ├── <header.h>
│   │   │       └── ...
│   │   └── src
│   │       ├── <source.cc>
│   │       └── ...
│   ├── ...
│   └── CMakeLists.txt
├── [tests]
│   ├── <test_suite_name>
│   │   ├── <test_name>
│   │   │   └── src
│   │   │       ├── <test_source.cc>
│   │   │       └── ...
│   │   ├── ...
│   │   └── CMakeLists.txt
│   └── ...
└── CMakeLists.txt
```
Once the directory structure matches the above, the top level `CMakelists.txt` should contain
```cmake
cmake_minimum_required(VERSION 3.24)

project(<project_name>)
include(${UROB_CMAKE_DIR}/urob_project.cmake)

urob_package(DEPS <dep>:<branch>=<url>...)
```
to create a standalone package. Calling `urob_package` is necessary to establish the project as a local dependency. It also serves as a place in which additional urob dependencies can be defined
by extending with parameter set `DEPS <name>:<branch>=<url>...`. CMake will treat these dependencies as Git repositories available to `FetchContent`. Undefined dependencies
are handled standardly through `find_package`. It also makes sure that the package can be properly installed and exported. 

`libs/CMakeLists.txt` is responsible for configuring built libraries by adding `urob_add_library` statements:
```cmake
urob_add_library(<library_name> "${CMAKE_CURRENT_SOURCE_DIR}/<library_name>" [DEFS <def>...] [DEPS <dep>::<component>...])
# ...
```
The optional arguments which can be supplied to the `urob_add_library` function call are:
* `DEFS` for adding compile definitions,
* `DEPS` followed by a list of dependencies which are expected to be namespaced.

Support for tests and examples is also available: they are added automatically if there is `tests`/`examples` folder within the root
directory and have been enabled (`BUILD_TESTING` and `BUILD_EXAMPLES` cache variables). Notice the CMakeLists.txt in respective locations.
Note that examples are not considered to be per-library.

---
Copyright &copy; 2022 syntakt
