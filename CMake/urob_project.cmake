########################################################################################
#
# Copyright 2022 syntakt
# All rights reserved
#
########################################################################################

include_guard()

include(FetchContent)
include(CTest)
include(${UROB_CMAKE_DIR}/urob_target.cmake)

option(BUILD_SHARED_LIBS "Build shared libraries." OFF)
option(BUILD_DOCUMENTATION "Build Doxygen documentation." OFF)
option(BUILD_EXAMPLES "Build examples" OFF)

add_compile_options("\
$<$<COMPILE_LANG_AND_ID:CXX,GNU>:-std=c++2a;-fno-rtti;-O3;-Wall;-Wextra;-Werror;-Wfatal-errors;-Wmultiple-inheritance;-Wvirtual-inheritance;-Wold-style-cast;-Wdelete-non-virtual-dtor;-Wno-non-template-friend>\
$<$<COMPILE_LANG_AND_ID:CXX,MSVC>:/std:c++20;/vms;/GR-;/O2;/Wall;/WX;/wd4003;/wd4061;/wd4266;/wd4371;/wd4458;/wd4514;/wd4623;/wd4626;/wd4668;/wd4686;/wd4706;/wd4710;/wd4711;/wd4820;/wd5027>
")

if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
	set(CMAKE_INSTALL_PREFIX "${CMAKE_BINARY_DIR}/install" CACHE PATH "" FORCE)
endif ()

if (BUILD_DOCUMENTATION)
	find_package(Doxygen REQUIRED)
	set(DOXYGEN_EXCLUDE_PATTERNS "*/tests/*")
	set(DOXYGEN_SHOW_USED_FILES "NO")
	set(DOXYGEN_OUTPUT_DIRECTORY "docs")
	doxygen_add_docs(docs "${CMAKE_CURRENT_SOURCE_DIR}" "${FETCHCONTENT_BASE_DIR}")
endif ()

function (urob_package)
	FetchContent_Declare(${PROJECT_NAME} URL "${CMAKE_CURRENT_SOURCE_DIR}" OVERRIDE_FIND_PACKAGE)

	cmake_parse_arguments(ARG "" "" "DEPS" ${ARGN})
	foreach (dep ${ARG_DEPS})
		string(FIND ${dep} "=" equals_pos)
		if (NOT ${equals_pos} EQUAL -1)
			string(SUBSTRING ${dep} 0 ${equals_pos} dep_pkg)
			set(dep_tag master)
			string(FIND ${dep_pkg} ":" colon_pos)
			if (NOT ${colon_pos} EQUAL -1)
				math(EXPR dep_tag_pos "${colon_pos} + 1")
				string(SUBSTRING ${dep} ${dep_tag_pos} equals_pos dep_tag)
				string(SUBSTRING ${dep} 0 ${colon_pos} dep_pkg)
			endif ()
			math(EXPR dep_url_pos "${equals_pos} + 1")
			string(SUBSTRING ${dep} ${dep_url_pos} -1 dep_url)
			FetchContent_Declare(${dep_pkg} GIT_REPOSITORY ${dep_url} GIT_TAG ${dep_tag} OVERRIDE_FIND_PACKAGE)
		endif ()
	endforeach ()

	set(lib_dir "${CMAKE_CURRENT_SOURCE_DIR}/libs")
	if (IS_DIRECTORY "${lib_dir}")
		add_subdirectory("${lib_dir}")
	endif ()

	set(project_version_major ${${PROJECT_NAME}_VERSION_MAJOR})
	get_property(target_list DIRECTORY "${lib_dir}" PROPERTY targets)
	foreach (target ${target_list})
		set_target_properties(${target} PROPERTIES VERSION ${PROJECT_VERSION}
		                                           SOVERSION ${project_version_major}
		                                           INTERFACE_${target}_MAJOR_VERSION ${project_version_major}
		                                           COMPATIBLE_INTERFACE_STRING ${project_version_major})
	endforeach ()

	write_basic_package_version_file(
		"${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}-config_version.cmake"
		VERSION "${PROJECT_VERSION}"
		COMPATIBILITY AnyNewerVersion
	)

	set(project_export ${PROJECT_NAME}_export)
	set(project_dest_lib ${PROJECT_NAME}/lib)
	install(TARGETS ${target_list}
	        EXPORT ${project_export}
	        ARCHIVE DESTINATION ${project_dest_lib}
	        LIBRARY DESTINATION ${project_dest_lib}
	        RUNTIME DESTINATION ${PROJECT_NAME}/bin
	        FILE_SET HEADERS DESTINATION ${PROJECT_NAME}/include)
	install(EXPORT ${project_export} FILE ${PROJECT_NAME}-config.cmake DESTINATION ${project_dest_lib}/cmake)
	export(EXPORT ${project_export} FILE "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}-config.cmake")
	install(FILES "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}-config_version.cmake" DESTINATION ${project_dest_lib}/cmake)

	set(test_dir "${CMAKE_CURRENT_SOURCE_DIR}/tests")
	if (${BUILD_TESTING} AND IS_DIRECTORY "${test_dir}")
		add_subdirectory("${test_dir}")
	endif ()

	set(example_dir "${CMAKE_CURRENT_SOURCE_DIR}/examples")
	if (${BUILD_EXAMPLES} AND IS_DIRECTORY "${example_dir}")
		add_subdirectory("${example_dir}")
	endif ()
endfunction ()
